--[SECTION] Inserting/Creating Records

INSERT INTO artists (name) VALUES ("Psy");
INSERT INTO artists (name) VALUES ("Linkin Park");
INSERT INTO artists (name) VALUES ("Backstreet Boys");


INSERT INTO albums (album_title, date_release, artist_id) VALUES (
	"Psy 6",
	"2012-1-1",
	1
);

INSERT INTO albums (album_title, date_release, artist_id) VALUES (
	"Meteora",
	"2003-3-25",
	2
);

INSERT INTO albums (album_title, date_release, artist_id) VALUES (
	"Millenium",
	"1999-5-18",
	3
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES (
	"Gangnam Style", 
	253, 
	"K-pop",
	1
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES (
	"Lying from you", 
	255, 
	"Rock",
	2
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES (
	"I Want It That Way", 
	333, 
	"Pop",
	3
);

-- [SECTION] Selecting/Retrieving Records

--Display all information about all albums

SELECT * FROM albums;

-- Display only the tile of all songs

SELECT song_name FROM songs;

-- Display titles and genres from songs

SELECT song_name, genre FROM songs;

-- Display the tile of all pop songs

SELECT song_name FROM songs WHERE genre = "Pop";

-- Display the title and length of Rock songs more than 3 minutes long

SELECT song_name, length FROM songs WHERE length > 259 AND genre = "Rock";

-- The "OR" keyword can also be used to forther modify SELECT conditions

-- [SECTION] Updating Records

UPDATE songs SET length = 240 WHERE id = 2;

--Removing the WHERE clause will update ALL rows/records in the TABLE

-- [SECTION] Deleting Records
DELETE FROM songs WHERE genre = "K-pop";

-- Removing the WHERE clause will delete ALL rows/records